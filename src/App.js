import {Component} from 'react';
import './App.css'

class App extends Component {
    state ={
        nums: [],
    }
    generateNums = () =>{
        const arrayFromFunction = [];
        const randomArray = (count, min, max) =>{
            if(count > (max - min)) return;
            const arr = [];
            while (count){
              const number =  Math.floor(Math.random() * (max - min + 1) + min);
              if(arr.indexOf(number) === -1){
                  arr.push(number);
                  count--;
              }
            }
            return arr;
        }
        const numbers = (randomArray(5, 5, 36)).sort((a, b) => a > b ? 1 : -1);
        const id = randomArray(5, 100, 199);
        for(let i = 0; i < numbers.length; i++){
            for(let j = 0; j < id.length; j++){
                arrayFromFunction.push({num: numbers[i], id: id[j]});
                i++;
            }
        };
        return arrayFromFunction
    }
    changeNumber = () => {
        const numbers = this.generateNums();
        this.setState({
            nums: this.state.nums.map((p)=>{
              for(let i = 0; i < numbers.length; i++){
                  return {p: numbers[i]}
              }
            })
        });
    }
    render() {
        this.state.nums = (this.generateNums());
        const Number = props => {
            return (
                <>
                    <li>
                        {props.number}
                    </li>
                </>
            )
        }
        const num = this.state.nums.map((number) => (
            <Number key ={number.id} number={number.num}/>
        ));
        console.log(this.state.nums);
        return (
            <div>
                <ul className="container">{num}</ul>
                <div className="container btn">
                    <button  onClick={this.changeNumber}>Change Number</button>
                </div>
            </div>
        );
    }
}

export default App;
